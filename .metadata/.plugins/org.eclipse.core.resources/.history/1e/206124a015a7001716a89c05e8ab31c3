/**
 * This file contains the main program flow for the word counter app. This file makes use of many
 * different libraries.
 * @file WordCounterApp.c
 * @author Mitchell Larson
 * @date 9/26/2017
 */

#include "WordCounterApp.h"
#include "Trim.h"
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include "Dictionary.h"

static void processWord(struct dictionary* dict,char* text);
static int readAndProcessFile(struct dictionary* dict,char* filename);
static void printResults(char* mode, struct dictionary* dict);

const int MAX_WORD_LENGTH = 32;

/**
 * This is the main program flow for counting words in a file. It initializes a dictionary, reads and
 * processes the file, and prints the results.
 * @param argc	The number of command line arguments.
 * @param argv	Comand line arguments[./lab3 <W or C> <path to file>]
 * @return	EXIT_ERROR is unsucessful, EXIT_SUCCESS if successful.
 */
int main(int argc, char* argv[]){
	clock_t begin = clock();
	int status = EXIT_SUCCESS;

	//check that there are 3 arguments.
	if (argc >= 4) {
		pid_t pid;
		//pid_t mypid;
		int numProcessors = atoi(argv[2]);



		if(pid < 0){
			printf("An error has occured while forking a process.");
		}else if(pid > 0){
			//parent process
			printf("Parent Process %d\r\n", getpid());
			while (numProcessors > 0) {
				pid = fork();
				numProcessors--;
			}
			waitpid(getpid(), NULL, 0);
			//Only start reading from files if all child processes are ready.
			if (numProcessors == 0) {

			}
		}else{
			//child process
			printf("Child Process %d\r\n",getpid());
			exit(0);
		}
	}
		/*
		struct dictionary* myDictionary = malloc(sizeof(struct dictionary));



		for (int i = 3; i < argc; i++) {
			int rdFileStatus = EXIT_FAILURE;

			//initalize dictionary, read file, print results.
			initialize(myDictionary);
			rdFileStatus = readAndProcessFile(myDictionary, argv[i]);
			if (rdFileStatus == EXIT_SUCCESS) {
				printResults(argv[1], myDictionary);
			}else{
				status = EXIT_FAILURE;
			}
			cleanUp(myDictionary);
		}
		free(myDictionary);
	}else{
		printf("Format <./Lab4> <W or C> <Num. of processes> <file1> <file2> <fileN>\r\n");
		status = EXIT_FAILURE;
	}

	clock_t end = clock();
	printf("CPU time used %f seconds\r\n", ((double)(end - begin) / CLOCKS_PER_SEC));*/
	return status;

}

/**
 * This function prints the results of the word count to the console. A command line
 * argument chooses the mode.
 * @param mode		the mode to display, W for word followed by count, C for count followed by word.
 * @param dict		the dictionary to print results for.
 */
static void printResults(char* mode, struct dictionary* dict) {
	if (mode != NULL && dict != NULL) {

		for (int i = 0; i < ll_size(dict->dictionary); i++) {
			struct worddata* word = getWord(dict, i);
			if (strcmp(mode, "W") == 0) {
				printf("%s - %d\r\n", word->word, word->count);
			} else if (strcmp(mode, "C") == 0) {
				printf("%d - %s\r\n", word->count, word->word);
			} else {
				printf("%s is not a valid print format",mode);
			}
		}
	}
}

/**
 * This function processes a word before adding it to a dictionary. Words are stripped of special
 * characters and then changed to upper case.
 * @param dict		dictionary to add the text to.
 * @param text		the text to add to the dictionary.
 */
static void processWord(struct dictionary* dict, char* text){
	char* word = text;

	char* strippedWord = stripNonText(word);
	char* upperCapsWord = toCaps(strippedWord);
	addWord(dict,upperCapsWord);

	free(strippedWord);
	free(upperCapsWord);
}

/**
 * This function reads a file word by word, and calls the processWord function for each
 * word. If any file reading errors occur, this function returns a failure code.
 * @param dict		The dictionary to add new words to.
 * @param filename	The filename for the file containing words to be read.
 * @return			EXIT_SUCCESS if successful, EXIT_FAILURE otherwise.
 */
static int readAndProcessFile(struct dictionary* dict, char* filename){
	FILE* fp;
	int returnStatus;
	bool fileDone = false;

	printf("File: %s\r\n",filename);
	//open the file. If The filename does not exist, indicate that illegal parameters were passed in.
	if ((fp = fopen(filename, "r")) == NULL) {
		printf("Invalid params");
		returnStatus = EXIT_ERROR;
		fileDone = true;
	}

	int returnValue = 0;

	while (!fileDone) {
		char word[MAX_WORD_LENGTH];
		returnValue = fscanf(fp, "%31s ", word);

		//handle each case for reading words.
		if (returnValue == 1) {
			processWord(dict,word);
		} else if (errno != 0) {
			printf("File scanning failed with an error");
			returnStatus = EXIT_ERROR;
			fileDone = true;
		} else if (returnValue == EOF) {
			returnStatus =  EXIT_SUCCESS;
			fileDone = true;
		} else {
			printf("No match to string");
			fileDone = true;
		}
	}
	fclose(fp);
	return returnStatus;
}
