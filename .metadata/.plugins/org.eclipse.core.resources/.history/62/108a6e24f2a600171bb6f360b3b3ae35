/**
 * This file contains the implementation of a dictionary data structure. Several functions are provided below which
 * allow for the addition of new words to a dictionary, and functions which allow words to be retrieved.
 * as well.
 * @file UnsortedDictionary.c
 * @author Mitchell Larson
 * @date 9/26/2017
 */

#include "Dictionary.h"
#include "linkedlist.h"
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

static struct worddata* initWordData(char* word);

/**
 * This function adds a word to the dictionary that is passed in. If the word exists, then the count of the word is incremented by 1. If a
 * does not exist, then the word is added to a new worddata structure that is added to the dictionary.
 * @param dict		a non-null pointer to a dictionary.
 * @param word		a non-null pointer to a word.
 */
void addWord(struct dictionary* dict, char* word) {
	//make sure the dictionary and word are not null
	if (word != NULL && dict != NULL) {

		struct worddata* wordData = findWord(dict, word);

		//if the word exists, increment it's count.
		if (wordData != NULL) {
			incrementCount(wordData);
		} else {
			//if the word does not exist, create a new word and add it to the dictionary.
			struct worddata* wordData = initWordData(word);

			if (wordData != NULL) {
				ll_add(dict->dictionary, wordData, sizeof(struct worddata));
			}

			free(wordData);
		}
	}
}


/**
 * This function destructs and frees all allocated memory associated with a dictionary structure.
 * @param dict		a non-null pointer to a dictionary.
 */
void cleanUp(struct dictionary* dict) {
	if(dict != NULL){
		ll_clear(dict->dictionary);
		free(dict->dictionary);
		//free(dict);
	}
}

void combineCounts(struct worddata* word){

}

/**
 * This function will search a dictionary structure for a specified word. The search is implemented by iterating through
 * the list until the word is found. If the word is not found, then NULL is returned to the caller.
 * @param dict		a non-null pointer to a dictionary.
 * @param word		a non-null pointer to a word.
 * @return			a pointer to the found worddata, or NULL if not found.
 */
struct worddata* findWord(struct dictionary* dict, char* word){
	if(dict != NULL && word != NULL){
		struct linkedListIterator* iterator = ll_getIterator(dict->dictionary);

		while(ll_hasNext(iterator)){
			struct worddata* wordData = ll_next(iterator);

			//if the word is found in the dictionary, free the iterator, and return the worddata structure.
			if(strcmp(word, wordData->word)==0){
				free(iterator);
				return wordData;
			}
		}
		free(iterator);
	}
	return NULL;
}

/**
 * This function returns the number of unique words in the dictionary
 * @param dict		a non-null pointer to a dictionary structure.
 * @return			The total number of words added to the dictionary. -1 if an error occurs.
 */
int getTotalWordCount(struct dictionary* dict){
	if(dict != NULL){
		return ll_size(dict->dictionary);
	}
	return -1;
}

/**
 * This function will return a worddata structure at a given index of a dictionary.
 * @param dict		a non-null pointer to a dictionary.
 * @param index		a valid integer index to retrieve.
 * @return			a pointer to the worddata found or NULL if not found.
 */
struct worddata* getWord(struct dictionary* dict, uint32_t index){
	if(dict != NULL && index >= 0 && index < ll_size(dict->dictionary)){
		return ll_get(dict->dictionary, index);
	}
	return NULL;
}

/**
 * This function increments the count of a word by 1.
 * @param word	a non-null pointer to a worddata structure.
 */
void incrementCount(struct worddata* word){
	if(word != NULL){
		word->count++;
	}
}

/**
 * This function initializes a dictionary structure which is just a wrapper around a ordinary
 * linked list. This is done to make the structure type clear, and to provide protection to
 * the functions in the Dictionary.c file. The dictionary struct is returned to the caller.
 * @param dict	a non-null pointer to a dictionary structure.
 */
void initialize(struct dictionary* dict){
	if(dict != NULL){
		struct linkedList* myList = malloc(sizeof(struct linkedList));
		ll_init(myList);
		dict->dictionary = myList;
	}
}

/**
 * This function intializes a wroddata structure with the word passed in. When this is done, the word is
 * copied to the worddata structure and its count is set to 1.
 * @param word		a non-null pointer to a word.
 * @return			a pointer to a worddata structure containing the word passed in. NULL is not enough memory exists.
 */
static struct worddata* initWordData(char* word){
	if(word != NULL){
		struct worddata* wordData = malloc(sizeof(struct worddata));

		if(wordData != NULL){
			strcpy(wordData->word, word);
			wordData->count = 1;
			return wordData;
		}
	}
	return NULL;
}
