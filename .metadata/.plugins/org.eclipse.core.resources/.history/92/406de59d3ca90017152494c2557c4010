/**
 * This file contains the main program flow for the word counter app. This file makes use of many
 * different libraries.
 * @file WordCounterApp.c
 * @author Mitchell Larson
 * @date 9/26/2017
 */

#include "WordCounterApp.h"
#include "Trim.h"
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include "Dictionary.h"

static void processWord(char* text, int pipes[], int numProcesses);
static int readAndProcessFile(char* filename, int pipes[], int numProcesses);
static void printResults(char* mode, struct worddata* word);
static void parent(char* argv[], int argc, int pToCPipes[], int cToPPipes[]);

const int MAX_WORD_LENGTH = 32;

/**
 * This is the main program flow for counting words in a file. It initializes a dictionary, reads and
 * processes the file, and prints the results.
 * @param argc	The number of command line arguments.
 * @param argv	Comand line arguments[./lab3 <W or C> <path to file>]
 * @return	EXIT_ERROR is unsucessful, EXIT_SUCCESS if successful.
 */
int main(int argc, char* argv[]){
	clock_t begin = clock();
	int status = EXIT_SUCCESS;

	//check that there are 3 arguments.
	if (argc >= 4) {
		//pid_t mypid;
		int numProcessors = atoi(argv[2]);
		pid_t pids[numProcessors];
		int pToCPipes[2 * numProcessors];	//pipes for process communication from parent to children
		int cToPPipes[2 * numProcessors];   //pipes for process communication from children to parent

		//initialize pipes
		/*for (int i = 0; i < numProcessors; i++) {
		    pipe(&pipes[2*i]);
		}*/

		for(int i = 0; i < numProcessors; i++){
			//initialize pipes
			pipe(&pToCPipes[2*i]);
			pipe(&cToPPipes[2*i]);

			if((pids[i] = fork()) < 0){
				printf("Error creating child");
			}else if(pids[i] == 0){
				//Child

				//Initialize a dictionary
				struct dictionary* myDictionary = malloc(sizeof(struct dictionary));
				initialize(myDictionary);

				close(pToCPipes[(2*i)+1]);			//Close unused write end
				close(cToPPipes[(2*1)]);			//Close unused read end

				char word[MAX_WORD_LENGTH + 1];

				while (read(pToCPipes[i * 2], word, MAX_WORD_LENGTH + 1) > 0) {
					printf("Child %d got %s\r\n", i, word);
					addWord(myDictionary,word);
				}

				printf("About to send data back to parent. Total words: %d\r\n",ll_size(myDictionary->dictionary));
				for(int j = 0; j < ll_size(myDictionary->dictionary); j++){
					struct worddata* sendWord = getWord(myDictionary, j);
					write(cToPPipes[(i*2)+1], sendWord, sizeof(struct worddata));
					free(sendWord);
				}

				close(cToPPipes[(i*2)]+1);			//Close write pipe, no more data

				cleanUp(myDictionary);
				free(myDictionary);

				printf("\r\nChild %d exiting\r\n", i);
				exit(0);
			} else {
				//Parent
				close(cToPPipes[(2*i)+1]); 		//Close unused write end
				if(i == (numProcessors -1)){
					parent(argv, argc, pToCPipes,cToPPipes);
					waitpid(-1, NULL, 0);
					sleep(10);
					printf("All children terminated");
				}

			}
		}


	}


		/*if(pid < 0){
			printf("An error has occured while forking a process.");
		}else if(pid > 0){
			//parent process
			printf("Parent Process %d\r\n", getpid());
			while (numProcessors > 0) {
				pid = fork();
				numProcessors--;
			}
			waitpid(getpid(), NULL, 0);
			//Only start reading from files if all child processes are ready.
			if (numProcessors == 0) {

			}
		}else{
			//child process
			printf("Child Process %d\r\n",getpid());
			exit(0);
		}
	}
		/*
		struct dictionary* myDictionary = malloc(sizeof(struct dictionary));



		for (int i = 3; i < argc; i++) {
			int rdFileStatus = EXIT_FAILURE;

			//initalize dictionary, read file, print results.
			initialize(myDictionary);
			rdFileStatus = readAndProcessFile(myDictionary, argv[i]);
			if (rdFileStatus == EXIT_SUCCESS) {
				printResults(argv[1], myDictionary);
			}else{
				status = EXIT_FAILURE;
			}
			cleanUp(myDictionary);
		}
		free(myDictionary);
	}else{
		printf("Format <./Lab4> <W or C> <Num. of processes> <file1> <file2> <fileN>\r\n");
		status = EXIT_FAILURE;
	}

	clock_t end = clock();
	printf("CPU time used %f seconds\r\n", ((double)(end - begin) / CLOCKS_PER_SEC));*/
	return status;

}

static void parent(char* argv[], int argc, int pToCPipes[], int cToPPipes[]) {


	for (int i = 3; i < argc; i++) {
		int rdFileStatus = EXIT_FAILURE;

		//initalize dictionary, read file, print results.
		//initialize(myDictionary);
		//rdFileStatus = readAndProcessFile(myDictionary, argv[i], pipes, atoi(argv[2]));
		rdFileStatus = readAndProcessFile(argv[i], pToCPipes, atoi(argv[2]));
		if (rdFileStatus == EXIT_SUCCESS) {
			//printResults(argv[1], myDictionary);
		} else {
			//status = EXIT_FAILURE;
		}
		//cleanUp(myDictionary);

	}
	//close pipes
	for (int i = 0; i < atoi(argv[2]); i++) {
		close(pToCPipes[(2 * i) + 1]);
	}

	printf("About to read data back in parent");

	//read data back from processes
	for(int i = 0; i < atoi(argv[2]); i++){
		struct worddata* word = NULL;
		while(read(cToPPipes[(i*2)], word, sizeof(struct worddata))>0){
			printf("Got %s from child\r\n",word->word);
			printResults(argv[2], word);
		}
	}
	//free(myDictionary);
}

/**
 * This function prints the results of the word count to the console. A command line
 * argument chooses the mode.
 * @param mode		the mode to display, W for word followed by count, C for count followed by word.
 * @param dict		the dictionary to print results for.
 */
static void printResults(char* mode, struct worddata* word) {
	if (mode != NULL && word != NULL) {
		if (strcmp(mode, "W") == 0) {
			printf("%s - %d\r\n", word->word, word->count);
		} else if (strcmp(mode, "C") == 0) {
			printf("%d - %s\r\n", word->count, word->word);
		} else {
			printf("%s is not a valid print format", mode);
		}
	}
}

/**
 * This function processes a word before adding it to a dictionary. Words are stripped of special
 * characters and then changed to upper case.
 * @param dict		dictionary to add the text to.
 * @param text		the text to add to the dictionary.
 */
static void processWord(char* text, int pipes[], int numProcesses){
	char* word = text;

	char* strippedWord = stripNonText(word);
	char* upperCapsWord = toCaps(strippedWord);

	//Decide which pipe to send the word down based on the first letter
	char firstChar[1];
	strncpy(firstChar, upperCapsWord,1);
	int processToUse = (upperCapsWord[0]) % numProcesses;

	write(pipes[(processToUse*2)+1], upperCapsWord, MAX_WORD_LENGTH + 1);
	//addWord(dict,upperCapsWord);

	free(strippedWord);
	free(upperCapsWord);
}

/**
 * This function reads a file word by word, and calls the processWord function for each
 * word. If any file reading errors occur, this function returns a failure code.
 * @param dict		The dictionary to add new words to.
 * @param filename	The filename for the file containing words to be read.
 * @return			EXIT_SUCCESS if successful, EXIT_FAILURE otherwise.
 */
static int readAndProcessFile(char* filename, int pipes[], int numProcesses){
	FILE* fp;
	int returnStatus;
	bool fileDone = false;

	printf("File: %s\r\n",filename);
	//open the file. If The filename does not exist, indicate that illegal parameters were passed in.
	if ((fp = fopen(filename, "r")) == NULL) {
		printf("Invalid params");
		returnStatus = EXIT_ERROR;
		fileDone = true;
	}

	int returnValue = 0;

	while (!fileDone) {
		char word[MAX_WORD_LENGTH];
		returnValue = fscanf(fp, "%31s ", word);

		//handle each case for reading words.
		if (returnValue == 1) {
			processWord(word,pipes,numProcesses);
			//sleep(1);
		} else if (errno != 0) {
			printf("File scanning failed with an error");
			returnStatus = EXIT_ERROR;
			fileDone = true;
		} else if (returnValue == EOF) {
			returnStatus =  EXIT_SUCCESS;
			fileDone = true;
		} else {
			printf("No match to string");
			fileDone = true;
		}
	}
	fclose(fp);
	return returnStatus;
}
