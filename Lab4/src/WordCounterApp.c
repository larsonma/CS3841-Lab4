/**
 * This file contains the main program flow for the word counter app. This version of the program makes use of
 * child processes to increase both user and CPU time speed. File IO is handled by the parent process and all
 * dictionary additions and queries are handled by the child processes.
 * @file WordCounterApp.c
 * @author Mitchell Larson
 * @date 10/3/2017
 */

#include "WordCounterApp.h"
#include "Trim.h"
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include "Dictionary.h"

static void processWord(char* text, int pipes[], int numProcesses);
static int readAndProcessFile(char* filename, int pipes[], int numProcesses);
static void printResults(char* mode, struct worddata* word);
static void parent(char* argv[], int argc, int pToCPipes[], int cToPPipes[]);
static void child(int process, int numProcessors, int pToCPipes[], int cToPPipes[]);

const static int MAX_WORD_LENGTH = 32;

/**
 * This is the main program flow for counting words in a file. It initializes a dictionary, reads and
 * processes the file, and prints the results.
 * @param argc	The number of command line arguments.
 * @param argv	Comand line arguments[./lab3 <W or C> <path to file>]
 * @return	EXIT_ERROR is unsucessful, EXIT_SUCCESS if successful.
 */
int main(int argc, char* argv[]){
	clock_t begin = clock();

	//check that there are 3 arguments.
	if (argc >= 4) {
		//pid_t mypid;
		int numProcesses = atoi(argv[2]);
		pid_t pids[numProcesses];
		int pToCPipes[2 * numProcesses];	//pipes for process communication from parent to children
		int cToPPipes[2 * numProcesses];   //pipes for process communication from children to parent

		for(int processNum = 0; processNum < numProcesses; processNum++){
			//initialize pipes
			pipe(&pToCPipes[2*processNum]);
			pipe(&cToPPipes[2*processNum]);

			//create new child processes
			if((pids[processNum] = fork()) < 0){
				printf("Error creating child");
			}else if(pids[processNum] == 0){
				//Child
				child(processNum,numProcesses,pToCPipes,cToPPipes);
			} else {
				//Parent
				//Close unused write end
				close(cToPPipes[(2*processNum)+1]);

				//if all processes have been created, begin file scanning
				if(processNum == (numProcesses -1)){
					parent(argv, argc, pToCPipes,cToPPipes);
				}
			}
		}
	}else{
		printf("Format is: ./Lab4 <W or C> <Num of Child Processes> <File1> <File2> ... <FileN>\r\n");
	}

	clock_t end = clock();
	printf("CPU time used %f seconds\r\n", ((double)(end - begin) / CLOCKS_PER_SEC));
	return EXIT_SUCCESS;
}

/**
 * This is a helper function for the child process. The job of the child is to create and monitor
 * a individual dictionary.This helps speed up word counting. The child reads words assigned to it
 * from the parent process and adds them to the dictionary. When the parent signals it is done
 * scanning files, the child returns the resulting worddata structures to the parent.
 */
static void child(int process, int numProcessors, int pToCPipes[], int cToPPipes[]) {
	//Initialize a dictionary
	struct dictionary* myDictionary = malloc(sizeof(struct dictionary));
	initialize(myDictionary);

	//close unused write pipes. Not closing will prevent parent from sending EOF
	for (int j = 0; j < numProcessors; j++) {
		close(pToCPipes[(2 * j) + 1]);
	}

	char word[MAX_WORD_LENGTH + 1];

	//Read incoming words from the parent and add them to the dictionary.
	while (read(pToCPipes[process * 2], word, MAX_WORD_LENGTH + 1) > 0) {
		addWord(myDictionary, word);
	}

	//Send words in dictionary back to the parent after reading EOF from parent pipe.
	for (int j = 0; j < ll_size(myDictionary->dictionary); j++) {
		struct worddata* sendWord = getWord(myDictionary, j);
		write(cToPPipes[(process * 2) + 1], sendWord, sizeof(struct worddata));
	}

	//Close write pipe, no more data
	close(cToPPipes[(process * 2)] + 1);

	cleanUp(myDictionary);
	free(myDictionary);

	fprintf(stderr,"Child Process %d finished.\r\n",process);

	exit(0);
}

/**
 * This is a helper function that defines the parent behavior. The parent scans each file input from
 * the user and scans it word by word. The words are pre-processed and then send to the corresponding
 * child process according to its first letter. When all files have been read, the parent will read
 * back the results from its children.
 */
static void parent(char* argv[], int argc, int pToCPipes[], int cToPPipes[]) {
	//read in all files
	for (int i = 3; i < argc; i++) {
		readAndProcessFile(argv[i], pToCPipes, atoi(argv[2]));
	}

	//close write pipes to children, indicating all files are done
	for (int i = 0; i < atoi(argv[2]); i++) {
		close(pToCPipes[(2 * i) + 1]);
	}

	//read data back from processes and print the results
	for(int i = 0; i < atoi(argv[2]); i++){
		struct worddata* word = malloc(sizeof(struct worddata));
		while(read(cToPPipes[i*2], word, sizeof(struct worddata))>0){
			printResults(argv[1], word);
		}
		free(word);
	}

	//Wait for all children to finish exiting
	waitpid(-1, NULL, 0);
}

/**
 * This function prints the results of the word count to the console. A command line
 * argument chooses the mode.
 * @param mode		the mode to display, W for word followed by count, C for count followed by word.
 * @param word		the worddata structure holding the result data.
 */
static void printResults(char* mode, struct worddata* word) {
	if (mode != NULL && word != NULL) {
		if (strcmp(mode, "W") == 0) {
			printf("%s - %d\r\n", word->word, word->count);
		} else if (strcmp(mode, "C") == 0) {
			printf("%d - %s\r\n", word->count, word->word);
		} else {
			printf("%s is not a valid print format", mode);
		}
	}
}

/**
 * This function processes a word before adding and then chooses a child process to send it to based on its
 * first letter. Words are stripped of special characters and then changed to upper case.
 * @param text			the text to add to the dictionary.
 * @param pipes[] 		an array of pipes used to send words to a child process.
 * @param numProcesses	the total number of available processes.
 */
static void processWord(char* text, int pipes[], int numProcesses){
	char* word = text;

	char* strippedWord = stripNonText(word);
	char* upperCapsWord = toCaps(strippedWord);

	//Decide which pipe to send the word down based on the first letter of the word
	char firstChar[1];
	strncpy(firstChar, upperCapsWord,1);
	int processToUse = (upperCapsWord[0]) % numProcesses;

	//send the word to the corresponding child process
	write(pipes[(processToUse*2)+1], upperCapsWord, MAX_WORD_LENGTH + 1);

	free(strippedWord);
	free(upperCapsWord);
}

/**
 * This function reads a file word by word, and calls the processWord function for each
 * word. If any file reading errors occur, this function returns a failure code.
 * @param filename		The filename for the file containing words to be read.
 * @param pipes[]		An array of pipes that can be used to communicate with child processes.
 * @param numProcesses	The number of available child processes.
 * @return				EXIT_SUCCESS if successful, EXIT_FAILURE otherwise.
 */
static int readAndProcessFile(char* filename, int pipes[], int numProcesses){
	FILE* fp;
	int returnStatus;
	bool fileDone = false;

	printf("Scanning file: %s\r\n",filename);
	//open the file. If The filename does not exist, indicate that illegal parameters were passed in.
	if ((fp = fopen(filename, "r")) == NULL) {
		printf("Invalid params");
		returnStatus = EXIT_ERROR;
		fileDone = true;
	}

	int returnValue = 0;

	while (!fileDone) {
		char word[MAX_WORD_LENGTH];
		returnValue = fscanf(fp, "%31s ", word);

		//handle each case for reading words.
		if (returnValue == 1) {
			processWord(word,pipes,numProcesses);
		} else if (errno != 0) {
			printf("File scanning failed with an error");
			returnStatus = EXIT_ERROR;
			fileDone = true;
		} else if (returnValue == EOF) {
			returnStatus =  EXIT_SUCCESS;
			fileDone = true;
		} else {
			printf("No match to string");
			fileDone = true;
		}
	}
	fclose(fp);
	return returnStatus;
}
